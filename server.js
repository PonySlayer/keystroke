var mahalanobis = require("mahalanobis");

/*var data = [
  [1,2,3],
  [1,5,6],
  [7,3,4],
  [2,3,0],
  [9,0,-5]
];

var distances = mahalanobis(data);

distances.forEach(function(distance,i){
  console.log("The distance for row " + i + " is " + distance);
});                    */

var fs = require('fs');
var util = require('util');
var express = require("express");
var app = express();
app.use(express.static('public'));
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/db', function(req,res){

   fs.readFile('db.json', 'utf8', function(err, data) { if (err) res.send('No DB found!');
	 res.send(data);
	});
});

app.get('/rawdb', function(req,res){

   fs.readFile('rawdb.json', 'utf8', function(err, data) { if (err) res.send('No DB found!');
	 res.send(data);
	});
}); 

app.post('/reg', function(req,res){
   var uarr = req.body.data;
   var uname = req.body.user;                         

   var resp = {regd: true, resp: uarr, uname: uname};

   var prom = new Promise(function(resolve, reject) {
       fs.readFile('db.json', 'utf8', function(err, data) { if (err) return reject(err); return resolve(data);});});
   var db = [];
   
   prom.then(
	function(data) {db = JSON.parse(data);}, 
	function(err) {resp.resp = 'No DB found! Creating...';}
    ).then(function() {
           db.push( {'user': uname, 'db': uarr});
           fs.writeFile('db.json', JSON.stringify(db), 'utf8', (err)=>{if (err) throw err; console.log('DB updated!');});
    });  

   fs.readFile('rawdb.json', 'utf8', function(err, data) { 
	var rawData;
	if (err) 
	  rawData = [];
	else
	  rawData = JSON.parse(data);
	rawData.push(req.body);
        fs.writeFile('rawdb.json', JSON.stringify(rawData), 'utf8', (err)=>{if (err) throw err; console.log('Raw DB updated!');});
	});

   res.send(JSON.stringify(resp));
});

app.post('/auth', function(req, res){
    var uarr = req.body.data;
    var uname = req.body.user;
    var db = [];
    var prom = new Promise(function(resolve, reject) {
       fs.readFile('db.json', 'utf8', function(err, data) { if (err) return reject(err); return resolve(data);});});

    var resp = {newUser: false, auth: false, resp: ''};
    prom.then( // getting DB
	function(data) {db = JSON.parse(data);}, 
	function(err) {resp.resp = 'No DB found!';}
    ).then(function() { // auth itself
	var uidx = db.findIndex( (l) => l.user === uname);
	if (uidx == -1) 
		resp.newUser = true;
	else {
		var datas = db[uidx].db;
		datas.shift();
		var avgs = datas.reduce((p, v)=>(v.map((w,i)=>w+(p==undefined?0:p[i]))), undefined).map((v)=>(v/datas.length));
		var sigmas = datas.reduce((p,v)=>v.map((w,i)=>(w-avgs[i])*(w-avgs[i])+(p==undefined?0:p[i])), undefined)
				.map((v)=>Math.sqrt(v/(datas.length-1)));
		console.log(sigmas);
//		datas.push(uarr);
//		var distances = mahalanobis(datas);
//		var distances = datas.map((v)=>v.reduce((q,w,j)=>q+Math.abs(w-uarr[j])/sigmas[j]));
		var distances = datas.map((v)=>v.map((w,j)=>Math.abs(w-uarr[j])/sigmas[j]));
		var avgdist = uarr.map((w,j)=>Math.abs(w-avgs[j])/sigmas[j]);
		console.log(distances);
		console.log(avgdist);
		var lambda = Math.sqrt(avgdist.reduce((p,v)=>p+v*v, 0));		
		console.log('lambda = ', lambda);
		if (lambda <= 3)
			resp.auth = true;
		console.log ('P <= ' + ( 4/9/lambda/lambda));
		resp.resp += distances.map(function(v,i) {return 'D('+((i==db.length)?'%THIS%':i)+') = ' + v;}).join('<br/>');
	}
	resp.resp += '<br/><br/>user ' + uname + ' authd [not rly]; \n\n' +  util.inspect(uarr);
    }).then(function() { // show response
	console.log(JSON.stringify(resp));
	res.send(JSON.stringify(resp));
    });
   fs.readFile('rawdb.json', 'utf8', function(err, data) { 
	var rawData;
	if (err) 
	  rawData = [];
	else
	  rawData = JSON.parse(data);
	rawData.push(req.body);
        fs.writeFile('rawdb.json', JSON.stringify(rawData), 'utf8', (err)=>{if (err) throw err; console.log('Raw DB updated!');});
	});
});


app.listen(8080);
console.log('App started');